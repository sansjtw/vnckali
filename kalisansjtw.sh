#!/bin/bash
read -p "是否已安装dialog和fortune软件包？(y/n)" -t 30 cho
case $cho in
   "n")
      apt install dialog
      apt install fortune
      fortune
      ;;
    *)
     ;;
   esac
#!/bin/bash
# using dialog to create a menu
red='\033[1;31m'
#红色
green='\033[1;32m'
#绿色
yellow='\033[1;33m'
#黄色
blue='\033[1;34m'
#蓝色
light_cyan='\033[1;96m'
#淡青色
reset='\033[0m'
#重置颜色
temp=$(mktemp -t test.XXXXXX)
temp2=$(mktemp -t test2.XXXXXX)
while [ 1 ] 
do 
  dialog --ok-label 选择 --nocancel --shadow --title "选择菜单" --menu "请使用↑↓←→或者触摸屏选择菜单\nQQ:3258918614" 25 60 20 1 "换国内源" 2 "下载ubuntu" 3 "下载图形桌面" 4 "直接进入ubuntu" 5 "下载novnc" 6 "下载kali程序(termux版，支持vnc)" 7 "设置和启动和kali" 8 "添加快捷程序" 0 "退出Exit" 2> $temp2
  if [ $? -eq 1 ] 
  then 
     break 
  fi  
  selection=$(cat $temp2)
  case $selection in
    1) fortune
      if [ -d /data/data/com.termux/files/usr/etc/termux/mirrors/china ]; then
   	  rm -rf /data/data/com.termux/files/usr/etc/termux/chosen_mirrors
	  ln -s /data/data/com.termux/files/usr/etc/termux/mirrors/china /data/data/com.termux/files/usr/etc/termux/chosen_mirrors
	  fi
	  sed -i 's@^\(deb.*stable main\)$@#\1\ndeb https://mirrors.bfsu.edu.cn/termux/termux-packages-24 stable main@' $PREFIX/etc/apt/sources.list && yes | pkg update 
	  sed -i 's@^\(deb.*stable main\)$@#\1\ndeb https://mirrors.bfsu.edu.cn/termux/termux-packages-24 stable main@' $PREFIX/etc/apt/sources.list && pkg update
	  printf "${green}按${blue}回车${yellow}键返回${reset}"
        read -r -p "" input ;;
    2) fortune
      dialog --title 提示 --msgbox "安装完成后，你以后也可以用命令 proot-distro login ubuntu 来代替此脚本进入Ubuntu" 50 50
        printf "${green}按${blue}回车${yellow}键继续${reset}"
        read -r -p "" input
        pkg install python -y
        pkg install python2 -y
        pkg install python3 -y
        pkg install git -y
        pkg install wget -y
        apt install xfce4 xfce4-goodies -y
        apt install openssh -y
        apt install proot-distro -y
        proot-distro install ubuntu
        proot-distro login ubuntu 
        fortune
        printf "${green}按${blue}回车${yellow}键返回${reset}"
        read -r -p "" input ;;
    3) dialog --title 提示 --msgbox "要在ubuntu中下载！否则可能会失败" 50 50
        fortune
        printf "${green}按${blue}回车${yellow}键继续${reset}"
        read -r -p "" input
        bash -c "$(curl https://gitee.com/sansjtw/vnckali/raw/vnc/xxvnc.sh)" 
        fortune
        read -r -p "${green}按${blue}回车${yellow}键返回${reset}" input ;;
    4) dialog --title 提示 --msgbox "你需要先下载完Ubuntu后再使用这进入" 50 50
       fortune
       proot-distro login ubuntu ;;
    5)  dialog --title 提示 --msgbox "需要将桌面和ubuntu安装后在Ubuntu里再安装" 50 50
        fortune
        wget https://gitee.com/sansjtw/vnckali/raw/vnc/hinovnc -P $PREFIX/bin/
        chmod +x $PREFIX/bin/hinovnc
        hinovnc 
        fortune
        printf "${green}按${blue}回车${yellow}键返回${reset}"
        read -r -p "" input ;;
    6)  dialog --title 提示 --msgbox "目前只支持在termux里安装kali" 50 50
        fortune
        bash -c "$(curl https://gitee.com/sansjtw/vnckali/raw/kali/kali)" 
        fortune
        printf "${green}按${blue}回车${yellow}键返回${reset}"
        read -r -p "" input ;;
    7) dialog --title 提示 --msgbox "先下载kali程序和镜像后再使用" 50 50
       fortune
       bash -c "$(curl https://gitee.com/sansjtw/vnckali/raw/kali/kalixx)" 
       break ;;
    8)  dialog --title 提示 --msgbox "添加完快捷方式后，使用命令 sansjtw 即可重新打开此菜单" 50 50
        fortune
        printf "${green}按${blue}回车${yellow}键继续${reset}"
        read -r -p "" input
        wget https://gitee.com/sansjtw/vnckali/raw/master/sansjtw -P $PREFIX/bin/
        chmod +x $PREFIX/bin/sansjtw
        echo ""
        fortune
        echo "添加，完成，下次使用请用sansjtw"
        printf "${green}按${blue}回车${yellow}键返回${reset}"
        read -r -p "" input ;;
    0)  break ;; 
    *)    dialog --msgbox "你好像啥也没选，回车退出" 50 50
          break
  esac 
done 
rm -f $temp 2> /dev/null 
rm -f $temp2 2> /dev/null