# kali vnc桌面安装器

#### 介绍
需要在ubuntu中使用安装该脚本

#### 功能
1.拥有安装ubuntu功能

2.安装ubuntu后，可再次使用该安装器安装kali桌面

#### 一键使用


```
bash -c "$(curl https://gitee.com/sansjtw/vnckali/raw/master/kalisansjtw.sh)"
```


#### 使用说明
自觉摸索

#### 图形化更新
![jtk](20221104_182903.png)
![kali](20221104_183102.png)